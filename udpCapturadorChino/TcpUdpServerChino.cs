﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace udpCapturadorChino
{
    class TcpUdpServerChino
    {
        private const int sampleTcpPort = 10064;
        public Thread TcpThread, UdpThread;



        public void TcpUdpServerChino()
        {
            try
            {
                //Starting the TCP Listener thread.
                TcpThread = new Thread(new ThreadStart(StartListen2));
                TcpThread.Start();
                Console.WriteLine("Started SampleTcpUdpServer's TCP Listener Thread!\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("An TCP Exception has occurred!" + e.ToString());
                TcpThread.Abort();
            }
        }
        public void StartListen2()
        {
            string ipDevice;
            char[] delimiters = { ' ', ':' };
            //Create an instance of TcpListener to listen for TCP connection.
            TcpListener tcpListener = new TcpListener(sampleTcpPort);
            IPHostEntry localHostEntry;
            try
                {
                    localHostEntry = Dns.GetHostByName(Dns.GetHostName());
                }
                catch (Exception)
                {
                    Console.WriteLine("Local Host not found"); // fail
                    return;
                }
            IPEndPoint localIpEndPoint = new IPEndPoint(localHostEntry.AddressList[0], sampleTcpPort);
            tcpListener.Start();
            //Program blocks on Accept() until a client connects.
            Socket soTcp = tcpListener.AcceptSocket();
            soTcp.Bind(localIpEndPoint);
            try
            {
                while (true)
                {
                    
                    Console.WriteLine("SampleClient is connected through TCP.");
                    Byte[] received = new Byte[2048];
                    int bytesReceived = soTcp.Receive(received, received.Length, 0);
                    IPEndPoint tmpIpEndPoint = new IPEndPoint(localHostEntry.AddressList[0], sampleTcpPort);
                    EndPoint remoteEP = (tmpIpEndPoint);
                    ipDevice = remoteEP.ToString().Split(delimiters)[0];
                    //String dataReceived = System.Text.Encoding.ASCII.GetString(received);
                    /*String dataReceived = Encoding.ASCII.GetString(received, 0, received.Length);
                    Console.WriteLine(dataReceived);
                    String returningString = "The Server got your message through TCP: " +
                    dataReceived;
                    Byte[] returningByte = System.Text.Encoding.ASCII.GetBytes
                    (returningString.ToCharArray());
                    //Returning a confirmation string back to the client.
                    soTcp.Send(returningByte, returningByte.Length, 0);
                    tcpListener.Stop();*/
                }
            }
            catch (SocketException se)
            {
                Console.WriteLine("A Socket Exception has occurred!" + se.ToString());
            }
        }
    }
}